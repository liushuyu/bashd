#ifndef _GNU_SOURCE
#define _GNU_SOURCE 1
#endif

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE 1
#endif

#include <sys/prctl.h>

// bash includes, order sensitive
// must be included using the following order
// clang-format off
#include "builtins.h"
#include "bashansi.h"
#include "command.h"
#include "shell.h"
#include "general.h"
#include "builtins/bashgetopt.h"
#include "variables.h"
#include "builtins/common.h"
// clang-format on

#include "bashd.h"
#include "bashd-rpc.h"

extern char *strerror();
extern char **environ;

int bashd_evalstring(const char *filename, char *string) {
  return evalstring(savestring(string), filename,
                    SEVAL_NONINT | SEVAL_NOHIST | SEVAL_RESETLINE);
}

char **bashd_get_array(const char *name) {
  SHELL_VAR *var = find_variable_no_invisible(name);
  WORD_LIST *array;
  size_t size = 1;
  char **ret;

  if (var == NULL) {
    return NULL;
  }

  array = array_to_word_list(array_cell(var));
  ret = (char **)calloc(1, sizeof(char *));
  while (array) {
    ret = (char **)reallocarray(ret, size, sizeof(char *));
    if (ret == NULL) {
      perror("Unable to allocate memory for reading array");
      free(ret);
      return NULL;
    }
    ret[size - 1] = array->word->word;
    size++;
    array = array->next;
  }

  // insert sentinel
  if (ret) {
    ret = (char **)reallocarray(ret, size + 1, sizeof(char *));
    ret[size] = NULL;
  }

  return ret;
}

char **bashd_list_locals(size_t *length) {
  SHELL_VAR **var = all_visible_variables();
  SHELL_VAR **cur = var;
  char **ret = NULL;
  size_t size = 1;
  if (var == NULL) {
    return NULL;
  }
  ret = (char **)calloc(1, sizeof(char *));
  while (*cur) {
    ret = (char **)reallocarray(ret, size, sizeof(char *));
    if (ret == NULL) {
      perror("Unable to allocate memory for reading array");
      free(ret);
      return NULL;
    }
    ret[size - 1] = (*cur)->name;
    size++;
    cur++;
  }

  // insert sentinel
  if (ret) {
    ret = (char **)reallocarray(ret, size + 1, sizeof(char *));
    ret[size] = NULL;
    *length = size - 1;
  }
  free(var);

  return ret;
}

const bool bashd_is_array(const char *name) {
  SHELL_VAR *var = find_variable_no_invisible(name);
  if (var == NULL) {
    return false;
  }
  return array_p(var) || assoc_p(var);
}

int bashd_sourcefile(const char *filename) { return source_file(filename, 0); }

const char *bashd_get_var(const char *name) {
  SHELL_VAR *var = find_variable_no_invisible(name);
  if (var == NULL) {
    return NULL;
  }

  return var->value;
}

int bashd_set_var(const char *name, char *value) {
  SHELL_VAR *var;

  if (legal_identifier(name)) {
    var = bind_variable(name, value, 0);
    if (var && (readonly_p(var) || noassign_p(var)))
      return -1;
    return var ? 0 : -1;
  }
  return -1;
}

void dispose_dynamic_variable(const char *name) {
  SHELL_VAR *var = NULL;
  if ((var = find_variable(name)) && var->value) {
    hash_dispose((HASH_TABLE *)var->value);
  }
}

int bashd_clean_reset_shell(bool use_env) {
  delete_all_contexts(shell_variables);
  delete_all_variables(shell_functions);

  initialize_shell_variables(use_env ? environ : NULL, 1);
  reinit_special_variables();
  set_pwd();
  return 0;
}

int bashd_builtin(WORD_LIST *list) {
  int opt, rval;
  SHELL_VAR **vars;

  rval = EXECUTION_SUCCESS;
  reset_internal_getopt();
  while ((opt = internal_getopt(list, "")) != -1) {
    switch (opt) {
      CASE_HELPOPT;
    default:
      builtin_usage();
      return (EX_USAGE);
    }
  }
  list = loptend;
  if (!list) {
    builtin_usage();
    return (EX_USAGE);
  }
  prctl(PR_SET_NAME, "bashd");
  rval = bashd_start_rpc(list->word->word);
  return (rval);
}

/* Called when `template' is enabled and loaded from the shared object.  If this
   function returns 0, the load fails. */
int bashd_builtin_load(char *name) { return (1); }

void bashd_builtin_unload(char *name) {}

char *bashd_doc[] = {"bashd server",
                     ""
                     "Launch bashd server",
                     (char *)NULL};

struct builtin bashd_struct = {
    "bashd",               /* builtin name */
    bashd_builtin,         /* function implementing the builtin */
    BUILTIN_ENABLED,       /* initial flags for builtin */
    bashd_doc,             /* array of long documentation strings. */
    "bashd [port number]", /* usage synopsis; becomes short_doc */
    NULL                   /* reserved for internal use */
};
