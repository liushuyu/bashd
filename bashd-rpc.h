#ifndef BASHD_RPC_H
#define BASHD_RPC_H

#include "jsonrpc-c.h"

int bashd_start_rpc(const char *socket_path);

#endif
