#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int bashd_evalstring(const char *filename, char *string);
int bashd_sourcefile(const char *filename);
const char *bashd_get_var(const char *name);
char **bashd_get_array(const char *name);
char **bashd_list_locals(size_t *length);
const bool bashd_is_array(const char *name);
int bashd_set_var(const char *name, char *value);
int bashd_clean_reset_shell(bool use_env);
