#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "bashd-rpc.h"
#include "bashd.h"
struct jrpc_server server;

cJSON *rpc_reset_shell(jrpc_context *ctx, cJSON *params, cJSON *id) {
  bashd_clean_reset_shell(false);
  return cJSON_CreateString("ok");
}

cJSON *rpc_list_locals(jrpc_context *ctx, cJSON *params, cJSON *id) {
  size_t length = 0;
  char **locals = bashd_list_locals(&length);
  char **cursor = locals;
  cJSON *list = cJSON_CreateArray();
  while (length > 0) {
    cJSON_AddItemToArray(list, cJSON_CreateString(*cursor));
    cursor++;
    length--;
  }
  free(locals);
  return list;
}

cJSON *rpc_quit(jrpc_context *ctx, cJSON *params, cJSON *id) {
  jrpc_server_stop(&server);
  return cJSON_CreateString("bye");
}

cJSON *rpc_set_var(jrpc_context *ctx, cJSON *params, cJSON *id) {
  char *name = NULL;
  char *value = NULL;
  if (cJSON_GetArraySize(params) != 2) {
    ctx->error_code = -32602;
    ctx->error_message = strdup("Invalid number of parameters");
    return cJSON_CreateNull();
  }
  name = params->child->valuestring;
  value = params->child->next->valuestring;
  if (bashd_set_var(name, value)) {
    ctx->error_code = -32003;
    ctx->error_message = strdup("Unable to set the string value");
    return cJSON_CreateNull();
  }

  return cJSON_CreateString("ok");
}

cJSON *rpc_get_var(jrpc_context *ctx, cJSON *params, cJSON *id) {
  char *name = NULL;
  const char *value;

  if (params == NULL || params->child == NULL) {
    ctx->error_code = -32602;
    ctx->error_message = strdup("Insufficient parameters");
    return cJSON_CreateNull();
  }
  if (params->child->type != cJSON_String) {
    ctx->error_code = -32602;
    ctx->error_message = strdup("Invalid parameters type");
    return cJSON_CreateNull();
  }
  name = params->child->valuestring;
  value = bashd_get_var(name);
  if (!value) {
    return cJSON_CreateNull();
  }

  return cJSON_CreateString(value);
}

cJSON *rpc_evalstring(jrpc_context *ctx, cJSON *params, cJSON *id) {
  if (params->child->type != cJSON_String) {
    ctx->error_code = -32602;
    ctx->error_message = strdup("Invalid parameters type");
    return cJSON_CreateNull();
  }

  if (bashd_evalstring("??", params->child->valuestring)) {
    ctx->error_code = -32000;
    ctx->error_message = strdup("Unable to evaluate the string");
    return cJSON_CreateNull();
  }

  return cJSON_CreateString("ok");
}

cJSON *rpc_evalfile(jrpc_context *ctx, cJSON *params, cJSON *id) {
  if (params->child->type != cJSON_String) {
    ctx->error_code = -32602;
    ctx->error_message = strdup("Invalid parameters type");
    return cJSON_CreateNull();
  }

  if (bashd_sourcefile(params->child->valuestring)) {
    ctx->error_code = -32001;
    ctx->error_message = strdup("Unable to evaluate the file");
    return cJSON_CreateNull();
  }

  return cJSON_CreateString("ok");
}

int bashd_start_rpc(const char *socket_path) {
  if (strnlen(socket_path, 256) > 108) {
    printf("Invalid UDS path\n");
    return 1;
  }
  if (jrpc_server_init(&server, socket_path) != 0) {
    printf("Failed to start RPC server\n");
    return 1;
  }

  jrpc_register_procedure(&server, rpc_reset_shell, "reset_shell", NULL);
  jrpc_register_procedure(&server, rpc_evalfile, "evalfile", NULL);
  jrpc_register_procedure(&server, rpc_evalstring, "evalstring", NULL);
  jrpc_register_procedure(&server, rpc_get_var, "get_var", NULL);
  jrpc_register_procedure(&server, rpc_set_var, "set_var", NULL);
  jrpc_register_procedure(&server, rpc_quit, "quit", NULL);
  jrpc_register_procedure(&server, rpc_list_locals, "locals", NULL);
  printf("bashd listening at %s...\n", socket_path);
  jrpc_server_run(&server);
  jrpc_server_destroy(&server);
  unlink(socket_path);
  return 0;
}
