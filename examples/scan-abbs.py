import socket
import json
import subprocess
import tempfile
import time
import logging
import os

from typing import Dict, List, Tuple, Any, Optional

BASHD_BIN = os.environ.get('BASHD_BIN') or './build/libbashd.so'
SPEC_DIR = os.environ.get('SPEC_DIR')
IGNORED_VARS = {"BASH", "BASHPID", "BASH_ALIASES", "BASH_ARGC", "BASH_ARGV", "BASH_ARGV0", "BASH_CMDS", "BASH_COMMAND", "BASH_EXECUTION_STRING", "BASH_LINENO", "BASH_SOURCE", "BASH_SUBSHELL", "BASH_VERSINFO", "BASH_VERSION", "COMP_WORDBREAKS", "DIRSTACK",
                "EPOCHREALTIME", "EPOCHSECONDS", "EUID", "GROUPS", "HISTCMD", "HOSTNAME", "HOSTTYPE", "IFS", "LINENO", "MACHTYPE", "OPTERR", "OPTIND", "OSTYPE", "PATH", "PPID", "PS4", "PWD", "RANDOM", "SECONDS", "SHELL", "SHLVL", "SRANDOM", "TERM", "UID",
                "PIPESTATUS", "_"}


def make_rpc_call(method, *args):
    wire = {"jsonrpc": "2.0", "method": method, "params": args}
    return json.dumps(wire).encode('utf-8')


def parse_rpc_response(data):
    wire = json.loads(data)
    error = wire.get("error")
    if error:
        raise RuntimeError(
            f'RPC error: {error.get("code")}: {error.get("message")}')
    return wire.get("result")


def recv_all(sock: socket.socket):
    buffer = b''
    while True:
        data = sock.recv(4096)
        buffer += data
        if data[-1] == 10:
            break
    return buffer


def parse_file(sock: socket.socket, filename: str) -> Dict[str, str]:
    sock.send(make_rpc_call('reset_shell'))
    parse_rpc_response(recv_all(sock))
    sock.send(make_rpc_call('evalfile', filename))
    parse_rpc_response(recv_all(sock))
    sock.send(make_rpc_call('locals'))
    local_vars = parse_rpc_response(recv_all(sock))
    local_vars_set = set(local_vars)
    local_vars_set = local_vars_set.difference(IGNORED_VARS)
    result = {}
    for v in local_vars_set:
        sock.send(make_rpc_call('get_var', v))
        resp = parse_rpc_response(recv_all(sock))
        result[v] = resp
    return result


def collect_spec(keyword: str) -> List[bytes]:
    if not SPEC_DIR:
        raise Exception('Please specify SPEC_DIR')
    output = subprocess.check_output(['find', SPEC_DIR, '-name', keyword or 'spec'])
    files = output.splitlines()
    return files


def run_all(spec: bool) -> Optional[List[Any]]:
    files = collect_spec(None if spec else 'defines')
    proc, sock = create_rpc_connection()
    all_vars = {}
    total = len(files)
    count = 0
    error = 0
    for f in files:
        count += 1
        # time.sleep(0.001)
        print(f'\r[{count}/{total}] Processing ...', end='', flush=True)
        path = os.path.join(SPEC_DIR, f.decode('utf-8'))
        try:
            all_vars[os.path.relpath(path, SPEC_DIR)] = parse_file(sock, path)
        except Exception as ex:
            error += 1
            print(f'\r[{count}/{total}] Failure: {path}: {ex}')
            if error > 20:
                print('Too many errors')
                sock.close()
                proc.kill()
                return None
    sock.send(make_rpc_call('quit'))
    print('')
    sock.close()
    proc.wait()
    return all_vars


def create_rpc_connection() -> Tuple[subprocess.Popen, socket.socket]:
    print('Creating connection to bashd rpc server...')
    sock = socket.socket(socket.AF_UNIX)
    name = tempfile.mktemp()
    proc = subprocess.Popen(
        ["bash", "-c", f"enable -f '{BASHD_BIN}' bashd; bashd {name}"])
    for _ in range(3):
        exitcode = proc.poll()
        if exitcode is not None:
            raise RuntimeError(f"bash exited with error: {exitcode}")
        try:
            sock.connect(name)
            print("Connected")
            return proc, sock
        except Exception:
            time.sleep(0.3)
            continue
    raise RuntimeError("Could not connect to the RPC server")


print('[ spec  ] Collecting variables ...')
with open('/tmp/all_vars.json', 'wt') as f:
    json.dump(run_all(True), f)

print('[defines] Collecting variables ...')
with open('/tmp/all_vars_def.json', 'wt') as f:
    json.dump(run_all(False), f)
